from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from collections import defaultdict
from tkinter import messagebox
from tkinter import ttk
import matplotlib.pyplot as plt
import plotly.graph_objs as go
import networkx as nx
import tkinter as tk
import plotly as py
import pymysql
import sys


class Scatter2D(object):
    
    def __init__(self,g,colorMap,labels):
        
        self.g = g
        self.colorMap = colorMap
        self.labels = labels
        
        ##Storing the names of each node
        ##labels=list(g.nodes)
        
        pos=nx.spring_layout(g)
        for i in pos:
            g.node[i]['pos']=pos[i]
            
            
        pos=nx.get_node_attributes(g,'pos')
        
        edge_trace = go.Scatter(
            x=[],
            y=[],
            line=dict(width=0.5,color='#888'),
            hoverinfo='none',
            mode='lines')
        
        for edge in g.edges():
            x0, y0 = g.node[edge[0]]['pos']
            x1, y1 = g.node[edge[1]]['pos']
            edge_trace['x'] += tuple([x0, x1, None])
            edge_trace['y'] += tuple([y0, y1, None])
        
        node_trace = go.Scatter(
            x=[],
            y=[],
            text=[],
            mode='markers',
            hoverinfo='text',
            marker=dict(
                showscale=True,
                colorscale='YlGnBu',
                color=[],
                size=[],
                colorbar=dict(
                    thickness=15,
                    title='Node Connections',
                    xanchor='left',
                    titleside='right'
                ),
                line=dict(width=2)))
        
        for node in g.nodes():
            x, y = g.node[node]['pos']
            node_trace['x'] += tuple([x])
            node_trace['y'] += tuple([y])
             
        for node, adjacencies in enumerate(g.adjacency()):  
            node_trace['marker']['size']+=tuple([len(adjacencies[1])+5])
            node_info = '# of connections: '+str(len(adjacencies[1]))
            node_trace['text']+=tuple([node_info])
        
        
        traceEdges=go.Scatter(x=edge_trace['x'],
                       y=edge_trace['y'],
                       mode='lines',
                       line=dict(color='rgb(90,90,90)', width=1),
                       hoverinfo='none'
                       )
        
        traceNodes=go.Scatter(x=node_trace['x'],
                       y=node_trace['y'],
                       mode='markers',
                       name='net',
                       marker=dict(symbol='circle-dot',
                                     size= node_trace['marker']['size'], 
                                     color=colorMap,
                                     line=dict(color='rgb(0,0,0)', width=1)
                                     ),
                       text=labels,
                       hoverinfo='text'
                       )

         #On vire les axes, la grille, tout ce qui ne nous interesse pas
        axis=dict(showline=False,zeroline=False,showgrid=False,
                  showticklabels=False,title='')
        
        layout=go.Layout(title= "Projet BDR, Graphe de noeuds en deux dimensions. Algorithme Fruchterman-Reingold",  
                         font=dict(size=12),showlegend=False,autosize=True,
                         width=1200,height=700,xaxis=go.layout.XAxis(axis),
                         yaxis=go.layout.YAxis(axis),hovermode='closest')
        
        final=[traceEdges, traceNodes]
        self.fig=go.Figure(data=final,layout=layout)
                    
        py.offline.plot(self.fig)

class Scatter3D(object):
    
    def __init__(self,g,colorMap,labels):
        
        self.g = g
        self.colorMap = colorMap
        self.labels=labels
        
        pos=nx.spring_layout(g,dim=3)
        N=len(g.nodes())
        
        layt=[]
        for i in pos:
            g.node[i]['pos']=pos[i]
            layt.append(pos[i])
        
        
        Xn=[layt[k][0] for k in range(N)]
        Yn=[layt[k][1] for k in range(N)]
        Zn=[layt[k][2] for k in range(N)]
        
        Xe=[]
        Ye=[]
        Ze=[]
        
        for e in g.edges():
            Xe+=[pos[e[0]][0],pos[e[1]][0],None]
            Ye+=[pos[e[0]][1],pos[e[1]][1],None]
            Ze+=[pos[e[0]][2],pos[e[1]][2],None]
            
        pos=nx.get_node_attributes(g,'pos')
        
        node_trace = go.Scatter(
            text=[],
            mode='markers',
            hoverinfo='text',
            marker=dict(
                showscale=True,
                colorscale='YlGnBu',
                color=[],
                size=[],
                colorbar=dict(
                    thickness=15,
                    title='Node Connections',
                    xanchor='left',
                    titleside='right'
                ),
                line=dict(width=2)))
        
        for node, adjacencies in enumerate(g.adjacency()):  
            node_trace['marker']['size']+=tuple([len(adjacencies[1])+6])
            node_info = '# of connections: '+str(len(adjacencies[1]))
            node_trace['text']+=tuple([node_info])
        
        
        traceEdges=go.Scatter3d(x=Xe,y=Ye,z=Ze,
                            mode='lines',
                            line=dict(color='rgb(80,80,80)',width=3),
                            hoverinfo='none'
                            )
        
        traceNodes=go.Scatter3d(x=Xn,
               y=Yn,
               z=Zn,
               mode='markers',
               name='actors',
               marker=dict(symbol='circle',
                             size=node_trace['marker']['size'],
                             color=colorMap,
                             colorscale='Viridis',
                             line=dict(color='rgb(50,50,50)', width=0.5)
                             ),
               text=labels,
               hoverinfo='text'
               )

        axis=dict(showbackground=False,
                  showline=False,
                  zeroline=False,
                  showgrid=False,
                  showticklabels=False,
                  title=''
                  )
        
        layout = go.Layout(
                 title="Projet BDR, Graphe de noeuds en trois dimensions. Algorithme Fruchterman-Reingold",
                 width=1200,
                 height=700,
                 showlegend=False,
                 scene=dict(
                     xaxis=dict(axis),
                     yaxis=dict(axis),
                     zaxis=dict(axis),
                ),margin=dict(t=100),
                hovermode='closest')
            
        data=[traceEdges, traceNodes]
        self.fig=go.Figure(data=data, layout=layout)

        py.offline.plot(self.fig)


class DBConnector(object):
    
    def __init__(self,ip,user,pswd,db):
        
        self.ip = ip
        self.user = user
        self.pswd = pswd
        self.db = db
        
        self.mdb = pymysql.connect(self.ip,self.user,self.pswd,self.db)     
        self.cursor = self.mdb.cursor()
        
    def doQuery(self,queryStatement,data):
        self.cursor.execute(queryStatement,data)
        return self.cursor.fetchall()




class App(object):
    
    def __init__(self,window):
        
        self.window = window

        ##Creating three separate pannels
        dbPanel = tk.Frame(window)
        queryPanel = tk.Frame(window)
        graphPanel = tk.Frame(window,
                           height = 700,
                           width=700,
                           bg='#ddddddddd')
        
        '''Panel de connection'''
        tk.Label(dbPanel,text = "Database connector : ").grid(row=0,
                column=0,columnspan=2,sticky='N')
        
        ##IP name
        tk.Label(dbPanel, text= "IP : ").grid(row=1,column=0,sticky='W')
        ipEntry = tk.Entry(dbPanel,width=10)
        ipEntry.grid(row=1,column=1)
        
        ##Username
        tk.Label(dbPanel, text= "Username : ").grid(row=2,column=0,sticky='W')
        userEntry = tk.Entry(dbPanel,width=10)
        userEntry.grid(row=2,column=1)
    
        ##Password
        tk.Label(dbPanel, text= "Password : ").grid(row=3,column=0,sticky='W')
        pswdEntry = tk.Entry(dbPanel,show="*",width=10)
        pswdEntry.grid(row=3,column=1)
        ##Database
        tk.Label(dbPanel, text= "Database : ").grid(row=4,column=0,sticky='W')
        dbEntry = tk.Entry(dbPanel,width=10)
        dbEntry.grid(row=4,column=1)
        
        
        '''Panel de Requêtes'''
        ##Loci
        tk.Label(queryPanel, text= "Loci : ").grid(row=0,column=0,sticky='W')
        lociNames=["All","Cell","Organ"]
        lociList = ttk.Combobox(queryPanel, values=lociNames,width=8,state='readonly')
        lociList.current(0)
        lociList.grid(row=0,column=1)
        
        ##Type cellulaire
        tk.Label(queryPanel, text = "Cell Type : ").grid(row=1,column=0,sticky='W')
        cellNames=["All","Bacteria","Fungi"]
        cellList = ttk.Combobox(queryPanel, values=cellNames,width=8,state='readonly')
        cellList.current(0)
        cellList.grid(row=1,column=1)
        
        ##Year interval
        tk.Label(queryPanel, text= "\n Year Interval :").grid(row=2,column=0,sticky='W')
        tk.Label(queryPanel, text= "\n YYYY-MM-DD").grid(row=2,column=1)
        
        tk.Label(queryPanel, text= "Min : ").grid(row=3,column=0)
        minYear = tk.Entry(queryPanel,width=10)
        minYear.grid(row=3,column=1)
        
        tk.Label(queryPanel, text= "Max : ").grid(row=4,column=0)
        maxYear = tk.Entry(queryPanel,width=10)
        maxYear.grid(row=4,column=1)
        
        ##PMID
        tk.Label(queryPanel, text="PMID : ").grid(row=5,column=0,sticky='W')
        pmid = tk.Entry(queryPanel, width=10)
        pmid.grid(row=5,column=1)
        
        ##Plotting options
        
        tk.Label(queryPanel, text="\nPlot-type").grid(row=6,columnspan=2)
        plotNames = ["Simple","2D-Web based","3D-Web based"]
        plotopt = ttk.Combobox(queryPanel,values=plotNames,width=16,state='readonly')
        plotopt.current(0)
        plotopt.grid(row=7,columnspan=2)
        
        ##Submit Button
        tk.Label(queryPanel, text='').grid(row=8)
        querySubmit = tk.Button(queryPanel, text="Submit")
        querySubmit.grid(rowspan=2,columnspan=2,sticky='S')
        
        '''Query Manager'''
        def getQuery(event):
            
            
            
            ##DB connection
            
            ip =ipEntry.get()
            user =userEntry.get()
            pswd=pswdEntry.get()
            db=dbEntry.get()
            
            mdb = DBConnector(ip,user,pswd,db)
            cursor = mdb.cursor
           
            
            ##Extract locitype
            if lociList.get() != "All":
                locitype =lociList.get()[0]
            else:
                locitype = 0
                              
            
            ##extract celltype
            if cellList.get() != "All":
                celltype = cellList.get()[0]
            else:
                celltype = 0
            
            ##Extract minyear
            if minYear.get() =='':
                cursor.execute("SELECT MIN(dates) FROM events")
                minyear = cursor.fetchone()
            else:
                minyear = minYear.get()
            
            ##Extract maxyear
            if maxYear.get()=='':
                cursor.execute("SELECT MAX(dates) FROM events")
                maxyear = cursor.fetchone()
            else:
                maxyear = maxYear.get()
                
            ##Send Query
            
            
            data = (locitype,celltype,minyear,maxyear)
            
            '''Querier'''
            ##Check if pmid value has been entered.
            if not pmid.get()=='':
                pmidnumber=pmid.get()
                data = (pmidnumber,locitype,celltype,minyear,maxyear)
                queryStatement = (
                    "SELECT targetEntity, sourceEntity, PUBMED_ID FROM Events WHERE PUBMED_ID = %s AND targetEntityType = %s AND sourceEntityType = %s AND dates BETWEEN %s AND %s;"       
            )
            else:
                data = (locitype,celltype,minyear,maxyear)
                queryStatement = (
                        "SELECT targetEntity, sourceEntity, PUBMED_ID FROM Events WHERE targetEntityType = %s AND sourceEntityType = %s AND dates BETWEEN %s AND %s;"       
                )
                
            cooccurences=mdb.doQuery(queryStatement,data)
          
            '''Network Builder'''
            
            ##Extracting nodes, edges and PMIDs from our query.
            targetNodes=[]
            sourceNodes=[]
            Edges=[]
            pubmedNodes=defaultdict(list)
            
            for (x,y,z) in cooccurences:
                Edges.append((x,y))
                if z not in pubmedNodes[x]:
                    pubmedNodes[x].append(z)
                if z not in pubmedNodes[y]:
                    pubmedNodes[y].append(z) 
                
                if x not in targetNodes:
                    targetNodes.append(x)
                if y not in sourceNodes:
                    sourceNodes.append(y)
            
            ##Building integrated graph
            g=nx.Graph()
            g.add_nodes_from(targetNodes)
            g.add_nodes_from(sourceNodes)
            g.add_edges_from(Edges)
            
            ##Assign a color to each node type through a color map
            colorMap = ['#33bfa7'] * len(targetNodes) + ['#40931d'] * len(sourceNodes)
                   
            ##Creation d'un graphe integre, sous nxDraw
            pos=nx.spring_layout(g)
            graph = plt.Figure(figsize=(7,7),dpi=100,)
            a = graph.add_subplot(111)
            plt.axis('off')
            
            nx.draw_networkx(g,pos=pos,ax=a,node_color=colorMap,font_size=6)
            
            ##On insere le graph dans son Panel
            canvas = FigureCanvasTkAgg(graph,master=graphPanel)
            ##Ceci surimpose le graph suivant sur le précédent!
            ##Tres mauvaise pratique...
            canvas.get_tk_widget().grid(row=1,column=0)
            
            '''Creating labels list for the plotly scatterplots
            these will display the PMID against the cell name.'''
            if plotopt.get() != plotNames[0]:
                labels=[]
                for n in g.nodes():
                    stringInput=str(n)+"\t PMIDs: "
                    pmids = pubmedNodes[n]
                    for i in pmids:
                        stringInput+=str(i)+" "
                    labels.append(stringInput)
            
                ##Redirige vers la creation d'un graphe sous plotly en 2D // 3D
                ##Optionel.
                if plotopt.get() == plotNames[1]:
                    Scatter2D(g,colorMap,labels)
                elif plotopt.get() == plotNames [2]:
                    Scatter3D(g,colorMap,labels)
                
                
            
        ##Binding the submit button to the querier.
        querySubmit.bind("<Button-1>",getQuery)
        
        ##Agencing the db, querier and graph in the root window
        dbPanel.grid(row=0,column=0)
        queryPanel.grid(row=1,column=0)
        graphPanel.grid(row=0,column=1,rowspan=2)
  
def main():
	window=tk.Tk()
	window.title("Micro-Projet BDR : Interface Graphique Utilisateur")
	App(window)
	window.mainloop()
main()



  
      

