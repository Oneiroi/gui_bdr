-- Creation de la database
CREATE DATABASE projet CHARACTER SET 'utf8';
GRANT ALL PRIVILEGES ON projet.* TO 'Oneiros'@'localhost' IDENTIFIED BY '';
USE projet;

------------------------------------------------------------------------------
---------------------------Partie I - Les entites ----------------------------
------------------------------------------------------------------------------
/*
Question 1 - Creez chacune de ces tables, puis importez leurs données à partir
des fichiers CSV.
*/

-- Creation des tables
CREATE TABLE IF NOT EXISTS Bacteria (
	pmid INT(10) NOT NULL,
	id VARCHAR(15) NOT NULL,
	`begin` INT(9) NOT NULL,
	`end` INT(9) NOT NULL,
	entity VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS Fungi (
	pmid INT(10) NOT NULL,
	id VARCHAR(15) NOT NULL,
	`begin` INT(9) NOT NULL,
	`end` INT(9) NOT NULL,
	entity VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS Tissues (
	pmid INT(10) NOT NULL,
	id VARCHAR(30) NOT NULL,
	`begin` INT(9) NOT NULL,
	`end` INT(9) NOT NULL,
	entity VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS Cells (
	pmid INT(10) NOT NULL,
	id VARCHAR(30) NOT NULL,
	`begin` INT(9) NOT NULL,
	`end` INT(9) NOT NULL,
	entity VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS dates(
	pmid INT(10),
	pubYEAR YEAR(4),
	pubDATE DATE
);

--Pour faire une description complete des tables presentes dans la base de donnees:
SELECT TABLE_NAME, COLUMN_NAME, DATA_TYPE, COLUMN_TYPE 
FROM information_schema.columns 
WHERE table_schema = 'projet' 
ORDER BY TABLE_NAME, ORDINAL_POSITION;

-- Importation des tables
LOAD DATA LOCAL INFILE 'C:/Users/bi-ne/Desktop/M1-Bioinfo/S8/BDR/micro_projet/data/bacteria.csv'
INTO TABLE Bacteria
	FIELDS TERMINATED BY ','
	LINES TERMINATED BY '\r\n'
	(pmid,id,begin,end,entity);


LOAD DATA LOCAL INFILE 'C:/Users/bi-ne/Desktop/M1-Bioinfo/S8/BDR/micro_projet/data/fungi.csv'
INTO TABLE Fungi
	FIELDS TERMINATED BY ','
	LINES TERMINATED BY '\r\n'
	(pmid,id,begin,end,entity);

LOAD DATA LOCAL INFILE 'C:/Users/bi-ne/Desktop/M1-Bioinfo/S8/BDR/micro_projet/data/tissues.csv'
INTO TABLE Tissues
	FIELDS TERMINATED by ','
	LINES TERMINATED BY '\r\n'
	(pmid,begin,end,id,entity);

LOAD DATA LOCAL INFILE 'C:/Users/bi-ne/Desktop/M1-Bioinfo/S8/BDR/micro_projet/data/cells.csv'
INTO TABLE Cells
	FIELDS TERMINATED BY ','
	ENCLOSED BY '"'
	LINES TERMINATED BY '\r\n'
	(pmid,begin,end,id,entity);

LOAD DATA LOCAL INFILE 'C:/Users/bi-ne/Desktop/M1-Bioinfo/S8/BDR/micro_projet/data/dates.csv'
INTO TABLE dates
	FIELDS TERMINATED BY ','
	LINES TERMINATED BY '\r\n'
	(pmid,pubYEAR,pubDATE);

---------------------------------------------------------------------------
/*
2-Etude retrospective du Microbiote bactérien
en utilisant les requetes suivantes
*/

-- Q: Affichez le nombre de publications par annee:
SELECT pubYEAR , COUNT(*) FROM Dates
	GROUP BY pubYEAR
	ORDER BY pubYEAR DESC;

---------------------------------------------------------------------------

--Q: En quelle année le pic des publications est atteint?


--Version simple, mais propre a mysql : */
SELECT pubYEAR , COUNT(*) FROM Dates
	GROUP BY pubYEAR
	ORDER BY pubYEAR DESC
	LIMIT 1;

--Version 'retors', mais applicable a tout SGBD :
SELECT pubYEAR as Annee, COUNT(*) as nombre FROM Dates
	GROUP BY pubYEAR
	HAVING count(*) = (
			SELECT MAX(occurences) FROM(
			SELECT COUNT(*) as occurences 
			FROM Dates
				GROUP BY pubYEAR
			) as sous_requete
		);

-----------------------------------------------------------------------------
/*
Q: Dans cette année là, quelle bactérie est apparue le plus dans les publications
(maximum d’occurrences) ? De même quel fungi ?
*/

--Version "liste"
SELECT Bacteria.entity, count(Bacteria.entity) as nombre FROM Bacteria, Dates
	WHERE Bacteria.pmid = Dates.pmid
	AND Dates.pubYEAR = 2000
	GROUP BY Bacteria.entity
	ORDER BY nombre DESC;
	-- E.coli 1237

SELECT Fungi.entity, count(Fungi.entity) as nombre FROM Fungi, Dates
	WHERE Fungi.pmid = Dates.pmid
	AND Dates.pubYEAR = 2000
	GROUP BY Fungi.entity
	ORDER BY nombre DESC;	
	-- S.cerevisiae 386.

--Version "retors"
SELECT Bacteria.entity as Entity, COUNT(Bacteria.entity) as Nombre FROM Bacteria, Dates
	WHERE Bacteria.pmid = Dates.pmid
	AND Dates.pubYEAR = 2000
	GROUP BY Bacteria.entity
	HAVING COUNT(Bacteria.entity) = (SELECT MAX(nombre) FROM(
		SELECT Bacteria.entity as nom, count(Bacteria.entity) as nombre FROM Bacteria, Dates
			WHERE Bacteria.pmid = Dates.pmid
			AND Dates.pubYEAR = 2000
			GROUP BY Bacteria.entity
		) AS sous_requete
	);

SELECT Fungi.entity as Entity, COUNT(Fungi.entity) as Nombre FROM Fungi, Dates
	WHERE Fungi.pmid = Dates.pmid
	AND Dates.pubYEAR = 2000
	GROUP BY Fungi.entity
	HAVING COUNT(Fungi.entity) = (SELECT MAX(nombre) FROM(
		SELECT Fungi.entity as nom, count(Fungi.entity) as nombre FROM Fungi, Dates
			WHERE Fungi.pmid = Dates.pmid
			AND Dates.pubYEAR = 2000
			GROUP BY Fungi.entity
		) AS sous_requete
	);

------------------------------------------------------------------------------

/*
Q: Quelle bactérie a été très étudiée en T1, puis celle en T2 
et enfin celle en T3?
*/

--Separer les tables Date en 3 pour simplifier le temps de requete.
-- on fait ici le choix de ne pas perdre d'entrees
-- après avoir regardé les dates distinctes:
SELECT DISTINCT pubYEAR from Dates;
-- on fait les intervalles en fonction...

-- T1 : années 1920/1925.
CREATE VIEW T1 AS
	SELECT * FROM Dates
	WHERE pubYEAR BETWEEN 1920 AND 1925;

-- T2 années 1949/1951.
CREATE VIEW T2 AS
	SELECT * FROM Dates
	WHERE pubYEAR BETWEEN 1949 AND 1951;

-- T3 année 2000.
CREATE VIEW T3 AS
	SELECT * FROM Dates
	WHERE pubYEAR = 2000;

SELECT Bacteria.entity, count(Bacteria.entity) as nombre
FROM Bacteria, T1
WHERE Bacteria.pmid = T1.pmid
GROUP BY Bacteria.entity
ORDER BY nombre DESC;
-- Treponema pallidum 25

SELECT Bacteria.entity, count(Bacteria.entity) as nombre
FROM Bacteria, T2
WHERE Bacteria.pmid = T2.pmid
GROUP BY Bacteria.entity
ORDER BY nombre DESC;
-- E.coli 68

SELECT Bacteria.entity, count(Bacteria.entity) as nombre
FROM Bacteria, T3
WHERE Bacteria.pmid = T3.pmid
GROUP BY Bacteria.entity
ORDER BY nombre DESC;
-- E.coli 1237

------------------------------------------------------------------------------
---------------------------Partie I - Les evenements -------------------------
------------------------------------------------------------------------------

/*
Utilisez ce même principe pour creer les tables...
*/

-- Co-occurence bacterie_tissus events_bo:
CREATE TABLE Events_bo as SELECT DISTINCT
	Bacteria.entity as sourceEntity,
	"B" as sourceEntityType,
	tissues.entity as targetEntity,
	"O" as targetEntityType,
	dates.pmid as PUBMED_ID,
	dates.pubDate as dates
FROM bacteria,tissues,dates
	WHERE bacteria.pmid = tissues.pmid
	AND bacteria.pmid = dates.pmid
	AND bacteria.entity IS NOT NULL;



-- cooccurences de fungi et tissues events_fo:
CREATE TABLE Events_fo as SELECT DISTINCT
	Fungi.entity as sourceEntity,
	"F" as sourceEntityType,
	tissues.entity as targetEntity,
	"O" as targetEntityType,
	dates.pmid as PUBMED_ID,
	dates.pubDate as dates
	FROM Fungi, tissues, dates
	WHERE fungi.pmid = tissues.pmid
	AND fungi.pmid = dates.pmid
	AND fungi.entity IS NOT NULL;

-- co-occurence bacterie_cellule events_bc:
CREATE TABLE events_bc as SELECT DISTINCT
	bacteria.entity AS sourceEntity,
	"B" AS sourceEntityType,
	cells.entity as targetEntity,
	"C" as targetEntityType,
	dates.pmid as PUBMED_ID,
	dates.pubDate as dates 
	FROM bacteria,cells, dates
	WHERE bacteria.pmid = cells.pmid
	AND bacteria.pmid = dates.pmid
	AND bacteria.entity IS NOT NULL;

-- co-occurence fungi_cellules events_fc:
CREATE TABLE events_fc AS SELECT DISTINCT
	Fungi.entity AS sourceEntity,
	"F" AS sourceEntityType,
	cells.entity AS targetEntity,
	"C" AS targetEntityType,
	dates.pmid as PUBMED_ID,
	dates.pubDate AS dates
	FROM fungi, cells, dates
	WHERE fungi.pmid = cells.pmid
	AND fungi.pmid = dates.pmid
	AND fungi.entity IS NOT NULL;

-----------------------------------------------------------------------------
/*
Fusionnez et intégrez ces tables dans une seule table d'évènements,
que vous nommerez EVENTS
*/

-- Fusion des trois tables
CREATE TABLE EVENTS AS SELECT * FROM Events_bo
	UNION SELECT * FROM Events_fo
	UNION SELECT * FROM Events_bc
	UNION SELECT * FROM Events_fc;

------------------------------------------------------------------------------
---------------------------Partie I - Application ----------------------------
------------------------------------------------------------------------------

/*
Determinez pour chaque tissu ou organe le nombre de bacteries avec
lesquelles il co-ocurre dans les publications. De meme, affichez
le nombre de bacteries par type cellulaire.
*/

-- nombre de co-occurence bacterie_tissus:
SELECT targetEntity, COUNT(sourceEntity) AS instances FROM Events
WHERE targetEntityType = 'O' AND sourceEntityType ='B'
	GROUP BY targetEntity
	ORDER BY instances DESC;

-- nombre de co-occurence bacterie_cellule:
SELECT targetEntity, COUNT(sourceEntity) AS instances FROM Events
WHERE targetEntityType = 'C' AND sourceEntityType = 'B'
	GROUP BY targetEntity
	ORDER BY instances DESC;

-----------------------------------------------------------------------------
/*
Affichez la liste des BACTERIES present dans chaque ORGANE
pendant l'intervalle T1, T2 et T3
*/

-- Intervalle T1 : de 1920 a 1925
SELECT targetEntity, GROUP_CONCAT(`sourceEntity` SEPARATOR ' / ') AS `Souce entities`
FROM Events
WHERE targetEntityType = 'O'
	AND sourceEntityType = 'B'
	AND EXTRACT(YEAR FROM dates) BETWEEN 1920 AND 1925
GROUP BY targetEntity;

-- Intervalle T2: de 1949 a 1951
SELECT targetEntity, GROUP_CONCAT(`sourceEntity` SEPARATOR ' / ') AS `Souce entities`
FROM Events
WHERE targetEntityType = 'O'
	AND sourceEntityType = 'B'
	AND EXTRACT(YEAR FROM dates) BETWEEN 1949 AND 1951
GROUP BY targetEntity;

-- Intervalle T3 2000
SELECT targetEntity, GROUP_CONCAT(`sourceEntity` SEPARATOR ' / ') AS `Souce entities`
FROM Events
WHERE targetEntityType = 'O'
	AND sourceEntityType = 'B' 
	AND EXTRACT(YEAR FROM dates) = 2000
GROUP BY targetEntity;

-------------------------------------------------------------------------------
/*
Meme chose que 1 et 2 concernant les fungi.
*/

-- nombre de co-occurence fungi tissu.
SELECT targetEntity, COUNT(sourceEntity) AS instances FROM Events
WHERE targetEntityType = 'O' AND sourceEntityType ='F'
	GROUP BY targetEntity
	ORDER BY instances DESC;

-- nombre de co-occurence fungi cellule
SELECT targetEntity, COUNT(sourceEntity) AS instances FROM Events
WHERE targetEntityType = 'C' AND sourceEntityType = 'F'
	GROUP BY targetEntity
	ORDER BY instances DESC;

------------------------------------------------------------------------------
/*
Affichez la liste des fungi presents dans chaque organe
pendant l'intervalle T1, T2 et T3
*/

-- Intervalle T1 : 1920 à 1925
SELECT targetEntity, GROUP_CONCAT(`sourceEntity` SEPARATOR ' / ') AS `Source entities`
FROM Events
WHERE targetEntityType = 'O'
	AND sourceEntityType = 'F'
	AND EXTRACT(YEAR FROM dates) BETWEEN 1920 AND 1925
GROUP BY targetEntity;

-- Intervalle T2: 1949 a 1951
SELECT targetEntity, GROUP_CONCAT(`sourceEntity` SEPARATOR ' / ') AS `Source entities`
FROM Events
WHERE targetEntityType = 'O'
	AND sourceEntityType = 'F'
	AND EXTRACT(YEAR FROM dates) BETWEEN 1949 AND 1951
GROUP BY targetEntity;

-- Intervalle T3: 2000
SELECT targetEntity, GROUP_CONCAT(`sourceEntity` SEPARATOR ' / ') AS `Source entities`
FROM Events
WHERE targetEntityType = 'O'
	AND sourceEntityType = 'F'
	AND EXTRACT(YEAR FROM dates) = 2000
GROUP BY targetEntity;

--------------------------------------------------------------------------
/*
Q : Que constatez vous?

A : Tout simplement AUCUNE co-occurence de fungi dans les organes.
+ aucune co-occurence avant 2000, bactérie et fungi confondus...
On vérifie donc la contraposée : fungi- cellule. et bacterie - cellule.

*/

-- Bacteries-CELLULE pendant les trois intervalles.

-- Intervalle T1 : 1920 à 1925
SELECT targetEntity, GROUP_CONCAT(`sourceEntity` SEPARATOR ' / ') AS `Source entities`
FROM Events
WHERE targetEntityType = 'C'
	AND sourceEntityType = 'B'
	AND EXTRACT(YEAR FROM dates) BETWEEN 1920 AND 1925
GROUP BY targetEntity;

-- Intervalle T1 : 1949 à 1951
SELECT targetEntity, GROUP_CONCAT(`sourceEntity` SEPARATOR ' / ') AS `Source entities`
FROM Events
WHERE targetEntityType = 'C'
	AND sourceEntityType = 'B'
	AND EXTRACT(YEAR FROM dates) BETWEEN 1949 AND 1951
GROUP BY targetEntity;

-- Intervalle T1 : 2000
SELECT targetEntity, GROUP_CONCAT(`sourceEntity` SEPARATOR ' / ') AS `Source entities`
FROM Events
WHERE targetEntityType = 'C'
	AND sourceEntityType = 'B' 
	AND EXTRACT(YEAR FROM dates) = 2000
GROUP BY targetEntity;

-- beaucoup plus de co-occurence Bacteries cellules!
-- toujours rien avant 2000.

---------------------------------------------------------------------------
--Fungi-CELLULE pendant les trois intervalles.

-- Intervalle T1 : 1920 à 1925
SELECT targetEntity, GROUP_CONCAT(`sourceEntity` SEPARATOR ' / ') AS `Source entities`
FROM Events
WHERE targetEntityType = 'C'
	AND sourceEntityType = 'F'
	AND EXTRACT(YEAR FROM dates) BETWEEN 1920 AND 1925
GROUP BY targetEntity;

-- Intervalle T2: 1949 a 1951
SELECT targetEntity, GROUP_CONCAT(`sourceEntity` SEPARATOR ' / ') AS `Source entities`
FROM Events
WHERE targetEntityType = 'C'
	AND sourceEntityType = 'F'
	AND EXTRACT(YEAR FROM dates) BETWEEN 1949 AND 1951
GROUP BY targetEntity;

-- Intervalle T3: 2000
SELECT targetEntity, GROUP_CONCAT(`sourceEntity` SEPARATOR ' / ') AS `Source entities`
FROM Events
WHERE targetEntityType = 'C'
	AND sourceEntityType = 'F'
	AND EXTRACT(YEAR FROM dates) = 2000
GROUP BY targetEntity;

-- Mieux!

/*
RECAPITULATIF:

1920' :	RIEN...
1950' :	RIEN...
2000 :	Bacteries:	04 Co-occurences dans 02 tissus
					25 Co-occurences dans 12 cellulles
					
		Fungi :		00 Co-occurences dans 00 tissus
					04 Co-occurences dans 03 cellules
*/
